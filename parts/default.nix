{ inputs, ... }:
{
  imports = [
    inputs.haskell-flake.flakeModule
    inputs.pre-commit-hooks-nix.flakeModule
    inputs.treefmt-nix.flakeModule
  ];
  perSystem = {
    imports = [
      ./devShells.nix
      ./pre-commit.nix
      ./treefmt.nix
    ];
  };
}
