{ config, pkgs, ... }:
{
  devShells = {
    default = pkgs.mkShell {
      name = "nixos";
      meta.description = "Standard Development Environment";
      inputsFrom = [
        config.haskellProjects.default.outputs.devShell
        config.pre-commit.devShell
        config.treefmt.build.devShell
      ];
      packages = builtins.attrValues { inherit (pkgs) just; };
    };
  };
}
