{
  pre-commit.settings.hooks = {
    commitizen.enable = true;
    nil.enable = true;
    statix.enable = true;
  };
}
