{
  treefmt = {
    flakeCheck = false;
    programs = {
      cabal-fmt = {
        enable = true;
      };
      hlint = {
        enable = true;
      };
      nixfmt-rfc-style = {
        enable = true;
      };
    };
    projectRootFile = "flake.nix";
  };
}
