{
  inputs = {
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
    haskell-flake.url = "github:srid/haskell-flake";
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    pre-commit-hooks-nix = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    systems.url = "github:nix-systems/default";
  };
  outputs =
    inputs:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import inputs.systems;
      imports = [ ./parts ];

      perSystem =
        {
          config,
          self',
          system,
          ...
        }:
        {
          _module.args.pkgs = import inputs.nixpkgs {
            inherit system;
            config.allowBroken = true;
          };
          haskellProjects.default = {
            autoWire = [
              "packages"
              "apps"
            ];
            packages = {
              websockets.source = "0.13.0.0";
            };
            devShell = {
              enable = true;
              # tools = hp: { inherit (hp) haskell-language-server; };
              hlsCheck.enable = true;
            };
          };
          packages.default = self'.packages.example;
        };
    };
}
